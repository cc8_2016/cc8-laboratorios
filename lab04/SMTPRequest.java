import java.io.* ;
import java.net.* ;
import java.util.* ;
public class SMTPRequest implements Runnable {
    protected Socket client;
    protected DataOutputStream output;
    protected BufferedReader input;

    protected final static String SMTP_SERVER_NAME = "CC8-MAIL";

    public SMTPRequest(Socket c) {
        client = c;
    }

    public void run() {
        try {
            input = new BufferedReader(new InputStreamReader(client.getInputStream()));
            output = new DataOutputStream(client.getOutputStream());
            output.writeBytes("220 " + SMTP_SERVER_NAME + " Simple Mail Transfer Service Ready\n");
            String str_aux;

            expectHELO();

            while(true) {
                str_aux = input.readLine();
                String [] tokens = str_aux.split(" ");
                String cmd = tokens[0];
                cmd = cmd.toUpperCase();
                //HELO, MAIL FROM, RCPT TO, DATA,., QUIT
                switch (cmd) {
                    case "MAIL":
                        if(tokens[1].toUpperCase().equals("FROM:")) {
                            output.writeBytes("250  OK");
                        } else {
                            output.writeBytes("500  Syntax error, command unrecognized");
                        }
                        break;
                    case "RCPT":
                        if(tokens[1].toUpperCase().equals("TO:")) {
                            output.writeBytes("250  OK");
                        } else {
                            output.writeBytes("500  Syntax error, command unrecognized");
                        }
                        break;
                    case "DATA":
                        output.writeBytes("354  Go ahead; end with <CRLF>.<CRLF>");
                        String data_str_line = input.readLine();
                        String data_str = data_str_line;
                        while(data_str.equals(".")) {
                            data_str += data_str_line;
                        }
                        break;
                    case "QUIT":
                        output.writeBytes("221 " + SMTP_SERVER_NAME + " Service closing transmission channel");
                        input.close();
                        output.close();
                        client.close();
                        break;
                    default:
                        output.writeBytes("502  Command not implemented\n");
                        // revisar rfc para ver cual es la respuesta por default
                }
            }




        } catch (Exception e) {

        }
    }

    public void expectHELO() throws IOException {
        String str_aux;
        while(true) {
            str_aux = input.readLine();
            if(str_aux.equals("HELO " + SMTP_SERVER_NAME)) {
                output.writeBytes("220 " + SMTP_SERVER_NAME + " at your server.");
                break;
            } else {
                output.writeBytes("503 HELO first");
            }
        }
    }


}
