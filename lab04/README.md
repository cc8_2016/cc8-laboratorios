# SMTP Server Side I

En este lab debe implementar la primera fase de un servidor SMTP básico. Su programa debe interpretar los comandos SMTP desde la perspectiva del Servidor. Está implementando el SMTP Server Side del Servidor.
Los comandos SMTP que se requieren son: HELO, MAIL FROM, RCPT TO, DATA,”.”, QUIT. Además debe seguir la secuencia de estados establecida por el protocolo.

Las pruebas se realizaran abriendo una conexión hacia su server y se enviarán "a mano" los comandos SMTP. Su servidor debe contestar con los códigos de respuesta adecuados, ya sea que el comando fue aceptado ó si ocurrió algún error.

Si el correo es aceptado, de momento el server no hace más que asignar un id único al correo y almacenar el contenido del mismo en un archivo de texto. Al finalizar el ingreso de un correo, su servidor debe retornar en pantalla el Id asignado al correo.


Tips: Recuerde las pruebas realizadas en clase. Haga pruebas contra un servidor SMTP y observe la secuencia de comandos. Debe replicar el comportamiento.
Lea el RFC 821 para saber bien la secuencia y sus excepciones
https://tools.ietf.org/html/rfc821
