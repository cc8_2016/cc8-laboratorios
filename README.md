# Laboratorios de Ciencias de la Computación 8
Listado de laboratorios:

1. Sockets TCP y UDP
2. Web Server
3. Web Server - part 2 (threadPool)
4. SMTP Server Side I
5. SMTP Server Side II
6. Capturando segmentos TCP con Wireshark
7. Ping
8. Ping parte 2
9. Link-State Routing Protocol (incompleto)
10. Repaso para parcial 2
11. Hackear red wifi con wpa con kali linux
