import java.io.* ;
import java.net.* ;
import java.util.* ;
import java.util.concurrent.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.nio.file.Files;

public final class WebServer {

    public static final String DEFAULT_ROOT_FILE = "index.html";
    public static final String CONFIG_FILE = "server.conf";
    public static final int DEFAULT_THREAD_POOL_SIZE = 2;

    public static void main(String argv[]) throws Exception {
        int port = 2407; // puerto por defecto
        int threadPoolSize = readThreadPoolSize();
        // se revisa si se recibio parametro, se asigna al puerto
        if(argv.length > 0)
            port = Integer.parseInt(argv[0]);

        if(argv.length > 1)
            threadPoolSize = Integer.parseInt(argv[1]);

        System.out.println("El server se iniciara en el puerto: " + port
                                + "con un threadPool size: " + threadPoolSize );
        ServerSocket server = new ServerSocket(port);
        ExecutorService executor = Executors.newFixedThreadPool(2);

        // se mantiene aceptando request
        while(true){
            System.out.println("Esperando cliente...");
            // espera por un cliente
            Socket client = server.accept();
            System.out.println("Conectado");

            // Construct an object to process the HTTP request message.
            HttpRequest request = new HttpRequest(client);
            executor.execute(request);
        }
    }

    /**
    *   Reads the thread pool size from the CONFIG_FILE
    *   It's expected the file contains in the first line the following format
    *   MaxThreads=n
    *   @return n readed from the file, if an error happen it will return the DEFAULT_THREAD_POOL_SIZE
    */
    public static int readThreadPoolSize() {
        File file = new File(CONFIG_FILE);
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String str = br.readLine();
            String poolSize = str.split("=")[1];
            return Integer.parseInt(poolSize.trim());
        } catch (Exception e) {
            return DEFAULT_THREAD_POOL_SIZE;
        }
    }
}
