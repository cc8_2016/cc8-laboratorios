# Web Server - parte 2
Multi-Threaded Web Server

En esta fase su servidor debe de ser capaz de procesar y servir un objeto solicitado por medio de un mensaje request. Si el objeto existe, debe construir el HTTP response conteniendo el objeto solicitado (en el caso del GET) y enviarlo como respuesta al cliente. Si el objeto no existe, debe armar una página por defecto indicando que la pagina u objeto no se encontró.

Adicional a lo anterior debe implementar un thread pool para limitar la cantidad de conexiones a su servidor. La modificación se debe realizar en la sección de código donde están aceptando las nuevas conexiones TCP e instanciando los threads que atenderá cada requerimiento. Deben proveer un archivo de configuración para poder establecer la cantidad de threads que su servidor puede soportar concurrentemente. El formato del archivo de texto es el siguiente
MaxThreads=<numero de threads>

En el siguiente link encontrara informa sobre como implementar un thread pool en java

[http://java.sun.com/docs/books/tutorial/essential/concurrency/pools.html](http://java.sun.com/docs/books/tutorial/essential/concurrency/pools.html)

Revisé las posibles opciones para armar un threadPool, una opción interesante es la `java.util.concurrent.ThreadPoolExecutor`
Recomendaciones cree diferentes archivos html para diferentes request, incluya en el html imágenes <img> para generar multiples requests.

## Preguntas
Cree una tabla comparativa del rendimiento (thread vs tiempo) al utilizar un Threadpool de 1 a 100 threads y Grafique los resultados.

1. ¿Qué sucede desde el punto de vista del usuario cuando se intenta abrir más conexiones que las disponibles en su webserver?
2. ¿El tiempo de respuesta decrese considerablemente con la cantidad de Threads?
3. ¿En que punto la cantidad de Thread ya no es significante?

# Articulos relacionados
http://www.drdobbs.com/parallel/use-thread-pools-correctly-keep-tasks-sh/216500409?pgno=1
