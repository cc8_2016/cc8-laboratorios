import java.io.* ;
import java.net.* ;
import java.util.* ;
import java.util.concurrent.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.nio.file.Files;

class HttpRequest implements Runnable {
    public static final String HTML_404 = "<HTML><H1>Error 404 -Page Not Found</H1></HTML>\n";
    public Socket client;
    public DataOutputStream output;
    public BufferedReader input;

    public HttpRequest(Socket client) {
        this.client = client;
    }

    public void run() {
        try {
            input = new BufferedReader(new InputStreamReader(client.getInputStream()));
            output = new DataOutputStream(client.getOutputStream());

            // read the first line request
            String headReq = input.readLine();
            System.out.println("-- " + headReq + " --");

            // get the resource path information
            String resource = getResourceFromRequest(headReq);

            // open the file
            File f = new File(resource);

            // print the absolute path
            System.out.println("Resource: " + f.getAbsolutePath());

            // check if the file exist or if not return 404 error
            if(f.exists()) {
                BufferedReader fbr = new BufferedReader(new FileReader(f));
                printHeader(200, f);
                Files.copy(f.toPath(), output);
                fbr.close();
            } else {
                return404(output);
            }

            // close connections
            output.flush();
            output.close();
            input.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private String getResourceFromRequest(String headReq) {
        String resource = "";
        if(headReq != null)
            resource = headReq.split(" ")[1];
        if(resource != null && resource.length() > 0) {
            if(resource.equals("/")) {
                resource = WebServer.DEFAULT_ROOT_FILE;
            } else {
                resource = resource.substring(1, resource.length());
            }
        }
        return resource;
    }

    private void printHeader(int code, File f) throws IOException {
        switch(code) {
            case 404:
                output.writeBytes("HTTP/1.1 404 Not Found\r\n");
                break;
            case 200:
                output.writeBytes("HTTP/1.1 200 OK\r\n");
                break;
        }
        String lastModified = DateFormat.getDateInstance(DateFormat.FULL)
                                            .format(new Date(f.lastModified()));

        output.writeBytes("Connection close\r\n");
        output.writeBytes("Date: " + getCurrDate() + "\r\n");
        output.writeBytes("Server: miServidor\r\n");
        output.writeBytes("Last-Modified: " + lastModified + "\r\n");
        output.writeBytes("Content-Length: " + f.length() + "\r\n");
        output.writeBytes("Content-Type: " + returnType(f) + "\r\n\r\n");
    }

    /**
    *   return the mime type of the given File
    */
    private String returnType(File f) {
        String name = f.getName();
        String extension = "";
        if(name.contains(".")) {
            String [] nameParts = name.split("\\.");
            extension = nameParts[nameParts.length-1];
        }

        extension = extension.toLowerCase();
        switch(extension) {
            case "html":
                return "text/html";
            case "css":
                return "text/css";
            case "js":
                return "application/javascript";
            case "json":
                return "application/json";
            case "ico" :
                return "image/x-icon";
            case "gif":
                return "image/gif";
            case "png":
                return "img/png";
            case "jpg": case "jpeg":
                return "img/jpeg";
            default:
                return "";
        }

    }

    /**
    *   send to the output stream the response for 404 error
    */
    private void return404(DataOutputStream output) throws IOException {
        output.writeBytes("HTTP/1.1 404 Not Found\n");
        output.writeBytes("Connection close\n");
        output.writeBytes("Date: " + getCurrDate() + "\n");
        output.writeBytes("Server: miServidor\n");
        output.writeBytes("Last-Modified: Mon, 22 Jun 1998 12:00:15 GMT\n");
        output.writeBytes("Content-Length: " + HTML_404.length() + "\n");
        output.writeBytes("Content-Type: text/html\n\n");
        output.writeBytes(HTML_404);
    }

    /**
    * Retorna la fecha actual.
    */
    public String getCurrDate() {
        //get current date time with Date()
        Date date = new Date();
        return DateFormat.getDateInstance(DateFormat.FULL).format(date);
    }
}
