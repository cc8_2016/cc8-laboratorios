# Capturando segmentos TCP con Wireshark
En esta hoja de trabajo estudiaremos el comportamiento del protoco TCP. Para ello analizaremos un grupo de segmentos TCP generados durante la transferencia de un archivo de 150KB, conteniendo el cuento de Alicia en el Pais de las maravillas.

## a) Capturando segmentos TCP
Para iniciar la captura de segmentos TCP debe descargar e instalar Wireshark.  Para facilitar su trabajo, antes de iniciar la Wireshark asegurese de no tener abierta ninguna aplicación que tenga conexiones abiertas. Configure WS para que capture los paquetes en modo “no promiscuo”, para evitar llenar la ventana de capturas con información que de momento no nos interesa. Con WS INACTIVO, realice lo siguiente:

* Cargue la página http://gaia.cs.umass.edu/wireshark-labs/alice.txt
* Descargue y almacene el archivo en su máquina.
* Ahora ingrese a http://gaia.cs.umass.edu/wireshark-labs/TCP-wireshark-file1.html
	- ![screnshot.PNG](https://bitbucket.org/repo/gaRXLE/images/2739526639-screnshot.PNG)
* Utilice el botón browse en esta pantalla para ingresar el path donde descargo el archivo “alice.txt”. No presione el botón Upload.
* Inicie WireShark y comience la captura de paquetes
* Ahora proceda con el upload del archivo de texto. Cuando la operación termine detenga la captura de paquetes de WireShark

Si lo desea, filtre el detalle de la captura de WireShark, de tal forma que solo se muestren los mensajes que utilizan el protocolo TCP.

## Conteste las siguientes preguntas:
1. ¿Cuál es el puerto abierto en su computadora mientras transmitía el archivo?
	- Puerto 39352
2. ¿Cuál es la IP de gaia.cs.umass.edu y en a qué puerto se está conectando su computadora?
	- Ip =128.119.245.12
	- Puerto = 80
3. ¿Cuál es el Sequence Number del segmento TCP SYN utilizado para iniciar la conexión? ¿Qué flag identifica que el segmento es el TCP SYN?
	- Flags: 0x002
	- Sequence Number = 0
4. ¿Cuál es el Sequence Number del SYNACK enviado por gaia.cs.umass.edu? ¿Cuál es el valor del Ack Number? ¿Cómo es que gaia.cs.umass.edu determino ese Ack Number?, ¿Qué valores van en los flags del segmento para este SYN  ACK?
	- Sequence Number = 0
	- Ack Number = 1
	- Determino el acknolege era 1 por que el byte que esperaba que se enviara
	- Flags: 0x012
5. ¿Cuál es el Sequence Number del segmento TCP que contiene el HTTP POST?
    - Sequence Number = 1
6. Considerando el segmento que contiene el HTTPS POST, como el primero de la conexión, ¿Cuáles son los Sequence Number de los primeros 6 segmentos?
    - 1 (POST)
    - 2897
    - 5793
    - 8689
    - 11585
    - 14481
    - 17377
7. En base a estos primeros 6 segmentos, estime el RTT, comparando los tiempos de envÃ­o de estos segmentos contra los Acks recibidos.
    - | seq time | seq #	| ack time | ack # | RTT      |
      |----------|----------|----------|-------|----------|
      | 2.388204 | 1	    | 2.467350 | 2897  | 0.079146 |
      | 2.388309 | 2897     | 2.467410 | 5793  | 0.079101 |
      | 2.388319 | 5793     | 2.467414 | 8689  | 0.079095 |
      | 2.388507 | 11585	| 2.467432 | 14481 | 0.078925 |
      | 2.467395 | 17377	| 2.576639 | 20273 | 0.109244 |
      | 2.467810 | 20273	| 2.576642 | 23169 | 0.108832 |
      | 2.467850 | 23169	| 2.576646 | 26065 | 0.108796 |

8. ¿Cuál es la longitud de estos primeros 6 segmentos?
    - 2,896 Bytes
9. Durante toda la transferencia, ¿Cuál fue el tamaño mínimo de la ventana del receiver?
    - ![window scaling.png](https://bitbucket.org/repo/gaRXLE/images/3739018439-window%20scaling.png)
    - Si se toma en cuenta el 3-way handshack la ventana en ese momento era el valor más pequeño 28,960 pero de ahí ya cuando se estaba transmitiendo los datos fue de 34,816
10. ¿Encontró alguna retransmisión durante la conexión?, si la encontró, ¿Cómo se dio cuenta de ello?
    - No
11. ¿Encontró indicios de algún Cumulative Ack?, muestre una “foto” del ack que demuestra esto.
    - ![cumulative_ack.png](https://bitbucket.org/repo/gaRXLE/images/202651896-cumulative_ack.png)
    - Llego un Ack 10137 y después el ack 14481 pero debería haber habido un ack 11585 por lo tanto se puede ver que utiliza el ack del 14481 como cumulative ack por que no reenvia ningún paquete.
12. Seleccione un segmento TCP de las capturas que obtuvo, luego en el menú de WireShark vaya a Statistics->TCP Stream Graph -> Time-Sequence-Graph(Stevens), opción que muestra un grafica representando los sequence number del segmento y el tiempo en el que fue enviado. En esta gráfica,
    - ![seq time.png](https://bitbucket.org/repo/gaRXLE/images/549415380-seq%20time.png)
    - ¿Se puede identificar donde comienza y termina la fase de Slow Start?
        * Entre t=0 y t=0.26 aproximadamente
    - ¿Se puede identificar donde comienza y termina la fase Congestion Avoidance (Additive Increase)?
        * Entre t=0.28 y t=0.65

## Entregable:
Debe entregar un archivo .zip o .rar con los siguientes documentos:
- Este documento con sus respuestas.
- El archivo de las capturas que realizo en WireShark.
