import java.io.* ;
import java.net.* ;
import java.util.* ;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public final class WebServer {

    public static void main(String argv[]) throws Exception {
        int port = 2407; // puerto por defecto

        // se revisa si se recibio parametro, se asigna al puerto
        if(argv.length > 0)
            port = Integer.parseInt(argv[0]);

        System.out.println("El server se iniciara en el puerto: " + port);
        ServerSocket server = new ServerSocket(port);

        // se mantiene aceptando request
        while(true){

            System.out.println("Esperando cliente...");
            // espera por un cliente
            Socket client = server.accept();
            System.out.println("Conectado");

            // Construct an object to process the HTTP request message.
            HttpRequest request = new HttpRequest(client);

            // Create a new thread to process the request.
            Thread thread = new Thread(request);

            // Start the thread.
            thread.start();

        }
    }
}


final class HttpRequest implements Runnable {
    public Socket client;
    public DataOutputStream output;
    public BufferedReader input;

    public static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    public HttpRequest(Socket client) {
        this.client = client;
    }

    public void run() {
        try {
            input = new BufferedReader(new InputStreamReader(client.getInputStream()));
            output = new DataOutputStream(client.getOutputStream());

            String headReq = input.readLine();
            System.out.println("-- " + headReq + " --");

            // respuesta de heades quemada
            output.writeBytes("HTTP/1.1 200 OK\n");
            output.writeBytes("Connection close\n");
            output.writeBytes("Date: Thu, 06 Aug 1998 12:00:15 GMT\n");
            output.writeBytes("Server: miServidor\n");
            output.writeBytes("Last-Modified: Mon, 22 Jun 1998 12:00:15 GMT\n");
            output.writeBytes("Content-Length: 17\n");
            output.writeBytes("Content-Type: text/html\n\n");

            // solo mandael contenido si es GET
            if(headReq.contains("GET")) {
                output.writeBytes("<HTML>Hola</HTML>\n");
            }
            output.flush();
            output.close();
            input.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
    * Retorna la fecha actual.
    */
    public String getCurrDate() {
        //get current date time with Date()
        Date date = new Date();
        return dateFormat.format(date);
    }
}
