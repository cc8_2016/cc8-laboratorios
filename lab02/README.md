# Web Server
## Multi-Threaded Web Server

En este laboratorio estaremos construyendo la primera fase de un Web Server. Nuestra versión atenderá requests HTTP en el puerto 2407. Las pruebas finales se realizarán desde el Web Browser, sin embargo durante el proceso de desarrollo podría utilizar sesiones de Telnet. Si su máquina se llama host.juanito, el URL en el browser sería:

```
http://host.juanito:2407/index.html
```

Si no coloca el texto `:2407` su browser intentará abrir una conexión al puerto por default, en este caso al puerto `80`.

Debe implementar un servidor Multithreading, de manera tal que pueda atender a múltiples usuarios a la vez. Recuerde las técnicas aprendidas en Ciencias de la Computación VII.

A continuación un esqueleto de su posible implementación en Java:

```Java
import java.io.* ;
import java.net.* ;
import java.util.* ;

public final class WebServer
{
	public static void main(String argv[]) throws Exception
	{
		// . . .
	}
}

final class HttpRequest implements Runnable
{
	// . . .
}
```

La clase WebServer contendría la implementación de un server socket que este aceptando conexiones TCP.

```Java
while (true) {
	// Listen for a TCP connection request.
	//. . .
}
```

Por cada conexión recibida debe crear un thread que se encargue de gestionar dicha conexión. Cada thread debe recibir como parámetro una referencia al socket que debe atender.

```Java
// Construct an object to process the HTTP request message.
HttpRequest request = new HttpRequest( ? );

// Create a new thread to process the request.
Thread thread = new Thread(request);

// Start the thread.
thread.start();
```

Cada thread debe analizar los http request que reciba. En esta versión debe implementar los commandos Get y Head.

```
GET /somedir/page.html HTTP/1.1
Host: www.someschool.edu
User-agent: Mozilla/4.0
Connection: close
Accept-language:fr
```

Luego de analizar el encabezado, su servidor debe procesar y servir el objeto que se le está solicitando. Para el ejemplo anterior la pagina “page.html”. Si el objeto existe, debe construir el HTTP response conteniendo el objeto solicitado (en el caso del GET) y enviarlo como respuesta al cliente. Si el objeto no existe, debe armar una página por defecto indicando que la pagina u objeto no se encontró.

Ejemplo de un HTTP Response:

```
HTTP/1.1 200 OK
Connection close
Date: Thu, 06 Aug 1998 12:00:15 GMT
Server: miServidor
Last-Modified: Mon, 22 Jun 1998 …...
Content-Length: 17
Content-Type: text/html
<HTML>Hola</HTML>
```
