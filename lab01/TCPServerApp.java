import java.net.*;
import java.io.*;

public class TCPServerApp {

    public static void main(String[] args) {
        int port = 2500; // default port
        if(args.length > 0)
            port = Integer.parseInt(args[0]);

        System.out.println("Iniciando servidor en puerto: " + port );

        try {
            ServerSocket server = new ServerSocket(port);
            System.out.println("Esperando conexion de un cliente...");
            Socket socket = server.accept();
            System.out.println("Cliente conectado");
            DataInputStream input = new DataInputStream(socket.getInputStream());
            PrintStream output = new PrintStream(socket.getOutputStream());
            String str;

            try {
                while(true) {
                    str = input.readLine();
                    if(str.toUpperCase().equals("Q")) {
                        break;
                    } else {
                        System.out.print(str + "->");
                        output.println(str.toUpperCase());
                        System.out.println(str.toUpperCase());
                    }

                }
                System.out.println("Cerrando server..");
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println("Ocurrio un error con el servidor");
            } finally {
                try {
                    socket.close();
                    input.close();
                    output.close();
                } catch(Exception ex) {
                    System.out.println("Error al cerrar los streams");
                }
            }
        } catch(IOException e) {
            System.out.println("Error iniciando el servidor.");
            System.exit(1);
        }
    }
}
