import java.net.*;
import java.io.*;

public class TCPClientApp {
    public static Socket serverSocket;
    public static BufferedReader br;
    public static DataInputStream input;
    public static PrintStream output;

    public static void main(String[] args) throws Exception {
        int port = 2500; // default port
        String ipServer = "localhost"; // default server

        // read ip and port number from arguments
        if(args.length == 2){
            try {
                ipServer = args[0];
                port = Integer.parseInt(args[1]);
            } catch(NumberFormatException e) {
                printHelp();
                System.exit(1);
            }
        }

        try {
            serverSocket = new Socket(ipServer, port);
            System.out.println("Conectado con el server: " + ipServer + ":" + port);
            br = new BufferedReader(new InputStreamReader(System.in));

            input = new DataInputStream(serverSocket.getInputStream());
            output = new PrintStream(serverSocket.getOutputStream());

            while(true) {
                System.out.print("Ingrese palabra: ");
                String str = br.readLine();
                output.println(str);

                if(str.toUpperCase().equals("Q")) {
                    break;
                } else {
                    System.out.println(input.readLine());
                }
            }
            System.out.println("Cerrando cliente..");
        } catch (Exception e) {
            System.out.println("Error en conexión al server ip: " + ipServer + ", puerto: " + port);
        } finally {
            System.out.println("Cerrando  conexión con el server...");
            input.close();
            output.close();
            serverSocket.close();
        }
    }

    public static void printHelp() {
        System.out.println("Uso: \n\tjava TCPClientApp host port");
    }
}
