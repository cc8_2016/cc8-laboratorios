# Sockets TCP y UDP

Este laboratorio tiene como objetivo investigar el uso de los Sockets TCP y UDP en Java. Debe implementar una aplicación Cliente/Servidor, donde el Cliente debe solicitar una cadena de caracteres al usuario, luego debe conectarse al servidor, enviarle la cadena de caracteres y esperar por la respuesta del Server, el cual al recibir la cadena simplemente debe cambiar todos sus caracteres a mayúsculas y retornar la nueva cadena al cliente.

Debe crear dos clases, una para el ClientApp y otra para el ServerAPP. Debido a que debe implementar esta funcionalidad usando sockets TCP y UDP, su entrega debe contener las clases:

* TCPClientAPP, TCPServerAPP
* UDPClientAPP, UDPServerAPP.

## Actividades:

1.	**Desde la perspectiva del programador, ¿Qué diferencias existen entre usar Sockets TCP y Sockets UDP?** Los sockets TCP poseen dos streams para poder enviar y recibir datos los cuales estan relacionados directamente con un solo cliente, en cambio con los sockets UDP podemos tener un servidor que atienda a varios clientes utilizando un mismo socket. Ademas que en UDP la respuesta se basa en usar el packete recibido anteriormente para obtener los datos de a donde se debe enviar la respuesta.

2. **¿Qué hace el método Accept de un TCP ServerSocket?** detiene la ejecución del programa hasta que algun cliente solicite la conexion y este la acepta retornando una referencia al socket del cliente.
