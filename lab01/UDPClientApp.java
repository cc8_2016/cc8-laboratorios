
import java.net.*;
import java.io.*;

public class UDPClientApp {
    public static void main(String[] args) throws Exception {
        String host = "localhost";
        int port = 2500;

        DatagramSocket client = null;
        try {
            client = new DatagramSocket();
        } catch(SocketException e) {
            System.out.println("Error conectando al server " + host + ":" + port);
            System.exit(1);
        }
        InetAddress ipAddr = null;
        try {
            ipAddr = InetAddress.getByName(host);
        } catch(UnknownHostException e) {
            System.out.println("Unknown Host");
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


        while(true) {
            byte[] rData = new byte[2048];
            byte[] sData = new byte[2048];
            System.out.print("Ingrese palabra: ");
            String str = br.readLine();
            if(str.toUpperCase().equals("Q")) {
                break;
            } else {
                sData = str.getBytes();
                DatagramPacket sPacket = new DatagramPacket(sData, sData.length, ipAddr, port);
                client.send(sPacket);

                DatagramPacket rPacket = new DatagramPacket(rData, rData.length);
                client.receive(rPacket);
                String strUp = new String(rPacket.getData());
                System.out.println(strUp);
            }
        }
        client.close();
        System.out.println("Saliendo..");
    }
}
