import java.net.*;
import java.io.*;

public class UDPServerApp {
    public static void main(String[] args) {
        int port = 2500; // default port
        if(args.length > 0)
            port = Integer.parseInt(args[0]);

        System.out.println("Iniciando servidor en el puerto: " + port);

        DatagramSocket server = null;
        try {
            server = new DatagramSocket(port);

        } catch(SocketException e) {
            System.out.println("Ocurrio un error al iniciar el server en el puerto: " + port);
            System.exit(1);
        }

        try {
            while (true) {
                byte[] rData = new byte[2048];
                byte[] sData = new byte[2048];
                DatagramPacket rPacket = new DatagramPacket(rData, rData.length);
                server.receive(rPacket);
                String str = new String( rPacket.getData());
                if(str.toUpperCase().equals("Q")) {
                    break;
                } else {
                    System.out.print(str + "->");
                    InetAddress rIpAddr = rPacket.getAddress();
                    int rPort = rPacket.getPort();
                    String strUp = str.toUpperCase();
                    sData = strUp.getBytes();
                    DatagramPacket sPacket = new DatagramPacket(sData,sData.length, rIpAddr, rPort);
                    server.send(sPacket);
                    System.out.println(strUp);
                }
            }
        } catch(IOException e) {
            System.out.println("Ocurrio un error con la conexion");
        } finally {
            server.close();
        }




    }
}
