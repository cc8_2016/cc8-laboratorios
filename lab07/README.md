# Ping
**Nota:** Se debe ejecutar en linux y con permisos de administrador(sudo)
## Descripcion Parte 1
Deberá construir y enviar paquetes utilizando IP e ICMP. Para ello construirán “a puro pulmón” los datagramas.
Su programa debe funcionar leyendo de la línea de comandos los argumentos
- Source Host
- Source Port
- Destination Host
- Destination Port

Si faltara alguno de los argumentos debe mostrar un mensaje de error.
Deben realizar una implementación de Ping. Para este laboratorio deben entregar hasta la fase de construir y enviar el datagrama. No es necesario que capturen y analicen la respuesta ICMP que reciben en base al requerimiento que están enviando. Pueden utilizar Perl o C.

**NO** pueden utilizar cosas como NetPacket,etc., en general cualquier herramienta que arme y envíe los datagramas por ustedes (Usar cosas como netpacket seria como hacer un programa que levanta una instancia del ping.exe. no habría reto). Su programa debe manejar las conexiones que el usuario solicite. Para su implementación deben utilizar RAW sockets y si es posible utilizar librerías para facilitar la codificación del header, como por ejemplo para el cálculo del checksum o para dar “formato” a los valores a enviar.

Si usan perl, a continuación un ejemplo del esqueleto del programa:

```Perl
#!/usr/local/bin/perl

use Socket;

socket(SOCKET, AF_INET, SOCK_RAW, 255) || die $!;
setsockopt(SOCKET, 0, 1, 1);

#ojo, las variables $srcHost, $srcPort, $dstHost, $dstPort no estan definidas, ustedes las tienen que leer de la linea de comandos
my $packet = headers($srcHost, $srcPort, $dstHost, $dstPort);
my $destination = pack('Sna4x8', AF_INET, $dstPort, $dstHost);
send(SOCKET,$packet,0,$destination);

sub headers {
    local($srcHost,$srcPort,$dstHost,$dstPort) = @_;
    #aqui tienen que hacer su magia
}

#para el calculo del checksum podrian usar una funcion como la siguiente:
sub checksum {
    my $msg = shift;
    my $length = length($msg);
    my $numShorts = $length/2;
    my $sum = 0;

    foreach (unpack("n$numShorts", $msg)) {
       $sum += $_;
    }

    $sum += unpack("C", substr($msg, $length - 1, 1)) if $length % 2;
    $sum = ($sum >> 16) + ($sum & 0xffff);
    return(~(($sum >> 16) + $sum) & 0xffff);
}
```
## Descripción Parte 2:
Ahora que ya ha construido los encabezados de IP e ICMP, la siguiente etapa es capturar las respuestas ICMP para cada Echo Request que envía su programa.

Su implementación debe enviar 10 Echo Request a una dirección IP especificada por el usuario. En pantalla debe mostrar los tiempos calculados para cada pareja Request/Reply. Deme mostrar el Round Trip Time y una estimación del tiempo de ida (RTT/2).

Por cada Echo Request, si recibe la respuesta, envie el siguiente request, si no recibe respuesta espere al menos 2 segundos antes de continuar con el siguiente envío y muestre en pantalla los caracteres “ ** “ indicando que no se obtuvo respuesta. (Prueben con el ping de su OS.)

## Pruebas
Para comprobar que están enviando y recibiendo algo, les sugerimos que utilicen algún sniffer que capture sus paquetes (i.e. Wireshark) Las pruebas pueden realizarlas comparando los datagramas que su aplicación genera con los datagramas generados por el comando Ping.

## Tips
Revisen, lean y entiendan cada parte del encabezado de IP e ICMP
Lean sobre la función [pack](http://perldoc.perl.org/functions/pack.html) de Perl

## Entrega
La entrega de la Parte 1 es Jueves 15 de Septiembre, 8:00 PM.

La entrega de la Parte 2 es Domingo 18 de Septiembre, 8:00 PM.

Deben enviar todos los archivos con extensión .pl o .c utilizados en un archivo comprimido llamado <Carnet>.zip.
**Aquellos proyectos que sean detectados como copia, tendrán una nota de -100 sobre este laboratorio.**
