#!/usr/local/bin/perl
use Socket;
use Time::HiRes qw(time);

#ojo, las variables $srcHost, $srcPort, $dstHost, $dstPort no estan definidas, ustedes las tienen que leer de la linea de comandos
$srcHost = $ARGV[0];
$srcPort = $ARGV[1];
$dstHost = $ARGV[2];
$dstHostAux = $dstHost;
$dstPort = $ARGV[3];

# $dst_host = $dstHost;
# check parameters
if(!defined $srcHost or !defined $srcPort or !defined $dstHost or !defined $dstPort) {
   print "Usage: $0 <source host> <source port> <dest host> <dest port>\n";
   exit;
} else {
	main();
}

sub main {
	print "Ping $dstHost\n";
	# si no se pone esto no funciona bien :/
	$srcHost = (gethostbyname($srcHost))[4];
	$dstHost = (gethostbyname($dstHost))[4];

	my $icmp_protocol = 1;
	socket(SOCKET, AF_INET, SOCK_RAW, $icmp_protocol) || die $!;
	setsockopt(SOCKET, 0, 1, 1);

	$seq_n = int rand(0xFFF0);
	$id = int rand(0xFFF0);

	my $destination = pack('Sna4x8', AF_INET, $dstPort, $dstHost);

	$packets = 10;
	$max_rtt = 0;
	$min_rtt = 0; # un numero muy grande para que asi el primero sea menor
	$avg_rtt = 0;
	for (my $i = 0; $i < 10; $i++) {
		$seq_n +=  $i;
		# print "Ping #$i $dstAux.. ";
		# generate the packet
		my $destination = pack('Sna4x8', AF_INET, $dstPort, $dstHost);
		my $packet = headers($srcHost, $srcPort, $dstHost, $dstPort, $id, $seq_n);

		$start_time = time();
		send(SOCKET,$packet,0,$destination) || die $!;

		while(true) {
			# read response
			eval {
				$SIG{ ALRM } = sub { die "ping timeout\n"};
				alarm 2; # se establece una alarma en 2 segundos
				recv(SOCKET,$res,60,0);
				alarm 0; # se desactiva la alarma

			};
			if($@) {
				die unless $@ eq "ping timeout\n";
				print "\t** Timeout 2000 ms\n";
				last;
			} else {
				@header = unpack('N5h2h2nnn', $res);
				$size = length($res);
				my ($ip1, $ip2, $ip3, $ip4, $ip5, $type, $code, $checksum, $r_id, $r_seq_n) = @header;
				# print "Header($size): $type, $code, $r_id, $r_seq_n\n";
				if($type == 0 && $code == 0) {
					if($r_id == $id && $r_seq_n == $seq_n) {
						$rtt = int ((time() - $start_time)*1000);
						$avg_rtt += $rtt;
						if($max_rtt < $rtt) { $max_rtt = $rtt; }
						if($min_rtt == 0) { $min_rtt = $rtt; }
						if($min_rtt > $rtt) { $min_rtt = $rtt; }
						$rtt2 = $rtt/2;
						print "\tReply from $dstHostAux RTT = $rtt ms, RTT/2 = $rtt2 ms  \n";
						last;
					}
				}
			}
		}
	}
	$avg_rtt /= $packets;
	print "Min RTT: $min_rtt ms\n";
	print "Max RTT: $max_rtt ms\n";
	print "Avg RTT: $avg_rtt ms\n";
}


sub headers {
	local($srcHost, $srcPort, $dstHost, $dstPort, $id, $seq_n) = @_;
	my $zero_cksum = 0;


	my $icmp_type   = 8;
	my $icmp_code   = 0;

	# h2 type
	# h2 code
	# n checksum
	# a4 extra data
	my $pkt_temp = pack('h2h2nnn', $icmp_type, $icmp_code, $zero_cksum, $id, $seq_n);

	# calculate the checksum
	my $checksum = &checksum($pkt_temp);

	# pack again with the checksum
	my $pkt = pack('h2h2nnn', $icmp_type, $icmp_code, $checksum, $id, $seq_n);
	return $pkt;
}

# para el calculo del checksum podrian usar una funcion como la siguiente:
sub checksum {
	my $msg = shift;
	my $length = length($msg);
	my $numShorts = $length/2;
	my $sum = 0;

	foreach (unpack("n$numShorts", $msg)) {
	   $sum += $_;
	}

	$sum += unpack("C", substr($msg, $length - 1, 1)) if $length % 2;
	$sum = ($sum >> 16) + ($sum & 0xffff);
	return(~(($sum >> 16) + $sum) & 0xffff);
}
