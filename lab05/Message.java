import java.util.ArrayList;
import java.sql.*;
import java.net.*;
import java.io.*;

/**
 * Represents a Message
 */
public class Message  {
    protected int id;
    protected ArrayList<String> recipients;
    protected String mailFrom;
    protected String message;

    public Message() {
        recipients = new ArrayList<String>();
        message = "";
    }

    public String setMailFrom(String from) {
        String username = Util.getUsername(from);
        String domain = Util.getDomain(from);
        if(Util.validateUsername(username) && Util.validateDomain(domain)){
            mailFrom = from;
            return SMTPRequest.RES_250;
        } else {
            return "552 Too much mail data.\r\n";
        }
    }

    public String addRecipient(String rcpt) {
        // 550 No such user here
        rcpt = rcpt.trim();
        String username = Util.getUsername(rcpt);
        String domain = Util.getDomain(rcpt);

        if(Util.validateUsername(username) && Util.validateDomain(domain)) {
            if(recipients.size() < 100) {
                // check if is in the same domain
                if(domain.equals(SMTPServer.SMTP_SERVER_NAME)) {
                    recipients.add(rcpt);
                    return SMTPRequest.RES_250;
                } else {
                    recipients.add(rcpt);
                    return SMTPRequest.RES_251;
                }
            } else{
                return SMTPRequest.RES_552;
            }
        } else {
            return "552 Too much mail data.\r\n";
        }
    }

    public void appendMessage(String line) {
        message += line;
    }

    /**
     * Save the message in the database or resend if its not local message
     */
    public void save() throws Exception {
        Statement stmt = SMTPServer.dBConnection.createStatement();
        String sql = " INSERT INTO emails (message) VALUES('" + message +  "'); ";
        stmt.executeUpdate(sql);
        ResultSet res = stmt.executeQuery("SELECT last_insert_rowid() AS id");
        res.next();
        int id = res.getInt("id");
        for (String to : recipients) {
            String domain = Util.getDomain(to);
            if(!domain.equals(SMTPServer.SMTP_SERVER_NAME)) {
                send(mailFrom, to, message);
            } else {
                sql = "INSERT INTO emails_from_to VALUES ('" + mailFrom + "', '" + to + "', " + id + "); ";
                stmt.executeUpdate(sql);
            }
        }
        SMTPServer.dBConnection.commit();
    }

    /**
     * Re-send a message to another server
     */
    public static void send(String from, String to, String message) throws Exception {
        // consult dns table to know the ip of the other server
        String hostname = Util.getDomain(to);
        String ip = "";
        Statement stmt = SMTPServer.dBConnection.createStatement();
        String query = "SELECT ip FROM dns WHERE hostname LIKE '" + hostname + "';";
        ResultSet res = stmt.executeQuery(query);
        if(res.next()) {
            ip = res.getString("ip");
            Socket s = new Socket(ip, SMTPServer.SMTP_PORT);
            // BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));

            DataOutputStream output = new DataOutputStream(s.getOutputStream());
            output.writeBytes("HELO " + SMTPServer.SMTP_SERVER_NAME + "\r\n");
            System.out.println("HELO " + SMTPServer.SMTP_SERVER_NAME + "\r\n");
            output.writeBytes("MAIL FROM: " + from + "\r\n");
            System.out.println("MAIL FROM: " + from + "\r\n");
            output.writeBytes("RCPT TO: " + to + "\r\n");
            System.out.println("RCPT TO: " + to + "\r\n");
            output.writeBytes("DATA\r\n");
            System.out.println("DATA\r\n");
            String lines[] = message.split("\n");
            for(String l: lines) {
                output.writeBytes(l+ "\r\n");
                System.out.println(l+ "\r\n");
            }
            output.writeBytes(".\r\n");
            System.out.println(".\r\n");
            output.writeBytes("QUIT\r\n");
            System.out.println("QUIT\r\n");

            // close streams
            // input.close();
            output.close();
            s.close();
        } else {
            throw new Exception("DNS error unknown hostname: " + hostname);
        }
    }
}
