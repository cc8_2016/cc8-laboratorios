import java.util.regex.*;
/**
 * Class to help the SMTPServer
 */
public class Util {
    public final static String OP1 = "<[^ ]+@.+([.][^ ]+)?>";
    public final static String OP2 = "[^ ]+@[^ ]+([.][^ ]+)?";
    public final static String OP3 = "[^<]+ <[^ ]+@.+([.][^ ]+)?>";
    public final static String OP4 = ".+ [^ ]+@.+([.][^ ]+)?";

    /**
     * Obtain the username from the input
     * @param  str an email that can have a Name and/or optional <>
     * @return     username
     */
    public static String getUsername(String str) {
        str = str.trim(); // delete all white space in front and the end
        return Util.split(str)[0];
    }

    public static String getDomain(String str) {
        str = str.trim();
        return Util.split(str)[1];
    }

    /**
     * Auxiliary method to split an email (username@domain)
     * @param str a email with the format following formats
     *            Name LastName <username@domain>
     *            Name LastName username@domain
     *            <username@domain>
     *            username@domain
     * @return an array with the [username, domain]
     */
    protected static String[] split(String str) {
        if(Pattern.matches(OP1, str)) {
            // <username@domain[.ext]>
            return str.substring(1, str.length() - 1).split("@");
        } else if (Pattern.matches(OP2, str)) {
            // username@domain[.ext]
            return str.split("@");
        } else if (Pattern.matches(OP3, str)) {
            // Nombre Apellido <username@domain[.ext]>
            return str.substring(str.indexOf('<') + 1,str.length() -1).split("@");
        } else if (Pattern.matches(OP4, str)) {
            // Nombre Apellido... username@domain[.ext]
            return str.substring(str.lastIndexOf(' ') + 1,str.length()).split("@");
        } else {
            return new String[]{"", ""};
        }
    }

    /**
    * Validate if a user fill the requirements of the RFC 821
    * @See RFC 821 Section 4.5.3. SIZES
    * @param user username to be validated
    */
    public static boolean validateUsername(String user) {
        return user.length() < 64;
    }

    /**
    * Validate if a domain fill the requirements of the RFC 821
    * @See RFC 821 Section 4.5.3. SIZES
    * @param domain to be validated
    */
    public static boolean validateDomain(String domain) {
        return domain.length() < 64;
    }
}
