import java.util.concurrent.*;
import java.io.* ;
import java.net.* ;
import java.util.* ;
import java.sql.*;

public class SMTPServer {
    public static int SMTP_PORT = 2025;
    public static int THREAD_POOL_SIZE = 2;
    public static final String DB_NAME = "smtp.db";
    public static String SMTP_SERVER_NAME = "crackmail.com";
    public static boolean DEBUG = true;

    protected static Connection dBConnection;

    public static void main(String[] args) {

        // readConfiguration form arguments
        try {
            readConfig(args);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }

        // print configuration
        System.out.println("El smtp server se iniciara con la siguiente config: ");
        System.out.println("\t- Server Name: " + SMTP_SERVER_NAME);
        System.out.println("\t- Port: " + SMTP_PORT);
        System.out.println("\t- ThreadPool size: " + THREAD_POOL_SIZE);

        try {
            openDB();
            ServerSocket server = new ServerSocket(SMTP_PORT);
            ExecutorService executor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

            while(true) {
                Socket s = server.accept();
                executor.execute(new SMTPRequest(s));
            }
        } catch(IOException e) {
            System.out.println("Ocurio un error con la conexion");
        } catch(SQLException e) {
            System.out.println("Ocurrio un error al abrir la base de datos");
        }
    }

    /**
     * Read configuration form arguments, it's expected to have a -flag followd
     * by the value tu asign
     * Flags: tps, port, name, debug
     * @param args [description]
     */
    public static void readConfig(String[] args) throws Exception {
        int i = 0;
        if(args.length%2 != 0)
            throw new Exception("Cantidad de parametros incorrecto");
        while(i < args.length) {
            if(args[i].equals("-tps")) {
                THREAD_POOL_SIZE = Integer.parseInt(args[++i]);
            } else if(args[i].equals("-port")) {
                SMTP_PORT = Integer.parseInt(args[++i]);
            } else if(args[i].equals("-name")) {
                SMTP_SERVER_NAME = args[++i];
            } else if(args[i].equals("-debug")) {
                DEBUG = !args[++i].toLowerCase().equals("false");
            }
            i++;
        }
    }

    public static void openDB() throws SQLException {
        try {
            Class.forName("org.sqlite.JDBC");
            dBConnection = DriverManager.getConnection("jdbc:sqlite:" + DB_NAME);
            dBConnection.setAutoCommit(false);
        } catch ( Exception e ) {
            System.out.println("Ocurrio un error al abrir la base de datos");
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }
}
