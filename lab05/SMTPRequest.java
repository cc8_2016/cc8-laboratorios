import java.io.* ;
import java.net.* ;
import java.util.* ;

public class SMTPRequest implements Runnable {

    public final static String RES_220 = "220 " + SMTPServer.SMTP_SERVER_NAME
                                    + " Simple Mail Transfer Service Ready\r\n";
    public final static String RES_221 = "221 " + SMTPServer.SMTP_SERVER_NAME + " Service closing transmission channel\r\n";
    public final static String RES_250 = "250 OK\r\n";
    public final static String RES_251 = "251 User not local; will forward\r\n";
    public final static String RES_354 = "354 Go ahead; end with <CRLF>.<CRLF>\r\n";
    public final static String RES_500 = "500 Line too long.\r\n";
    public final static String RES_502 = "502 Command not implemented\r\n";
    public final static String RES_503 = "503 HELO first\r\n";
    public final static String RES_552 = "552 Too many recipients.\r\n";

    protected Socket client;
    protected DataOutputStream output;
    protected BufferedReader input;
    protected String full_cmd;

    protected Message message;

    public SMTPRequest(Socket c) {
        client = c;
        message = new Message();
    }

    public void run() {
        try {
            // open input and output stream and send a wellcome
            openStreams();
            output.writeBytes(RES_220);

            // The firs step is expect for a HELO
            expectHELO();

            // loop until QUIT
            while(true) {
                full_cmd = input.readLine();
                log(full_cmd);
                if(full_cmd.length() < 512) {
                    // Support of HELO, MAIL FROM, RCPT TO, DATA, ., QUIT
                    if(full_cmd.toUpperCase().startsWith("MAIL FROM:")) {
                        message = new Message();
                        String mailFrom = full_cmd.substring("MAIL FROM:".length(), full_cmd.length()).trim();
                        output.writeBytes(message.setMailFrom(mailFrom));
                        log("added mailFrom: " + mailFrom.trim());
                    } else if(full_cmd.toUpperCase().startsWith("RCPT TO:")) {
                        String rcpt = full_cmd.substring("RCPT TO:".length(), full_cmd.length());
                        output.writeBytes(message.addRecipient(rcpt));
                    } else if(full_cmd.toUpperCase().startsWith("DATA")) {
                        expectDATA();
                    } else if(full_cmd.toUpperCase().startsWith("QUIT")) {
                        closeStreams();
                        break; // exit infinit loop
                    } else {
                        output.writeBytes(RES_502);
                    }
                } else {
                    output.writeBytes(RES_500);
                }
            }
        } catch (Exception e) {
            System.out.println("Ocurrio un error en una de las conexiones.");
            e.printStackTrace();
        }
    }

    /**
     * Print in the STDOUT a loggin for debugging if DEBUG is active
     * @param str text should be output
     */
    public void log(String str) {
        if(SMTPServer.DEBUG)
            System.out.println(str);
    }

    /**
     * Open the input and output Streams using the socket
     */
    public void openStreams() throws IOException {
        input = new BufferedReader(new InputStreamReader(client.getInputStream()));
        output = new DataOutputStream(client.getOutputStream());
    }

    /**
     * Send closing channel message and close streams
     * and the socket
     */
    public void closeStreams() throws IOException {
        output.writeBytes(RES_221);
        input.close();
        output.close();
        client.close();
    }

    /**
     * Function that receive multiple lines for the data part of the message
     * until the input will be a . (point)
     */
    public void expectDATA() throws IOException {
        output.writeBytes(RES_354);
        String data_str_line = input.readLine();
        String data_str = data_str_line;
        while(!data_str_line.startsWith(".")) {
            data_str_line = input.readLine();
            if(data_str_line.length() < 1000) {
                if(!data_str_line.startsWith("."))
                data_str += data_str_line + "\n";
            } else {
                output.writeBytes(RES_500);
            }
        }
        output.writeBytes(RES_250);
        message.appendMessage(data_str);
        try {
            message.save();
        } catch (Exception e) {
            System.out.println("Error al guardar el mensaje en la base de datos.");
        }
        log("Message: " + message);
    }

    /**
     * Function that expect a HELO SMTP_SERVER_NAME and return when receive
     * that message.
     */
    public void expectHELO() throws IOException {
        String full_cmd;
        while(true) {
            full_cmd = input.readLine();
            if(full_cmd.startsWith("HELO ")) {
                output.writeBytes("220 " + SMTPServer.SMTP_SERVER_NAME + " at your service.\r\n");
                break;
            } else {
                output.writeBytes(RES_503);
            }
        }
    }

}
