# SMTP Server Side II
En este laboratorio debe implementar la segunda fase de un servidor SMTP básico. Este módulo es el encargado de almacenar los correos y de abrir una conexión SMTP hacia otro servidor. Está implementando el SMTP Client Side del Servidor. Este trabajo se realizará en grupos de dos personas.

Los comandos SMTP que se requieren son:

* HELO
* MAIL FROM
* RCPT TO
* DATA
* "."
* QUIT

Además debe seguir la secuencia de estados establecida por el protocolo.

Debe configurar la estructura de almacenamiento de correos en su servidor. Para ello debe contar con un listado de usuarios registrados, y debe diseñar un esquema de almacenamiento de correos por usuario(Archivos planos, una base de datos, etc). Si su servidor recibe un correo para uno de sus usuarios, simplemente debe guardar el correo en la estructura correspondiente. Si uno de los destinatarios no pertenece a los usuarios del servidor, entonces debe proceder a abrir una conexión con el servidor asociado a dicho usuario, y enviar el correo utilizando el protocolo SMTP.
El formato de direcciones de correo será el siguiente:

```
<usuario>@<dominio>
```
Para poder resolver el dominio, por ejemplo si su servicio se llama `ewoks.com` deben instalar un servicio DNS en su máquina y configurarlo como el servidor por defecto para su tarjeta. (Podrían probar el SimpleDNS, OpenDNS o cualquier otro servidor.) Se recomienda que prueben su servidor con el servidor de otro grupo. Deben agregar las entradas necesarias para poder resolver los diferentes dominios de los grupos con quienes vayan a conectarse.

Para las pruebas, con Telnet se estará abriendo una conexión a su servidor. Se escribirán los comandos SMTP y luego su servidor debe procesar la información recibida. Si el correo es para un usuario del servidor, almacenar el correo, sino, abrir una conexión con el servidor especificado por el dominio del correo y enviar los comandos SMTP correspondientes.

Luego de este laboratorio, tendremos un SMTP Server básico completo.

**Tips:** Recuerde las pruebas realizadas en clase. Haga pruebas contra un servidor SMTP y observe la secuencia de comandos. Debe replicar el comportamiento.


En el RFC indica que los correos puede venir de las siguientes formas:

* Nombre.. <correo@domain>
* Nombre.. correo@domain
* <correo@domain>
* correo@domain

## Base de datos
Se utilizo una base de datos de sqlite con las siguientes entidades:

* **dns:**: hostname, ip
* **emails**: id, message
* **emails_from_to**: from, to, message_id
* **registered_users**: username, alias

## Compilar

```
make
```

**NOTA**: si se utiliza en linux se debe descomentar la linea del CLASSPATH para linux y comentar la de windows en el archivo `Makefile`

## Ejecutarlo

```
sh smtp_server.sh [-port n] [-tps n] [-name str]
```

* `-port`: configura en que puerto se ejecutara el SMTP server _(default: 25)_
* `-tps`: configura el tamaño del ThreadPool _(default: 2)_
* `-name`: configura que nombre tendra el SMTP Server _(default: CC8-MAIL)_
* `-debug`: configura si se mostrara en la terminal información de los comandos procesados _(default: true)_
