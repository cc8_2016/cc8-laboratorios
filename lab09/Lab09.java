import java.util.ArrayList;

public class Lab09 {
    public static ArrayList<Node> N;

    public static void main(String[] args) {
        Node u = new Node("u");
        Node v = new Node("v");
        Node w = new Node("w");
        Node x = new Node("x");
        Node y = new Node("y");
        Node z = new Node("z");


        u.addTransition(v, 2);
        u.addTransition(x, 2);
        u.addTransition(w, 5);

        v.addTransition(w, 3);
        v.addTransition(x, 2);

        x.addTransition(u, 1);
        x.addTransition(v, 2);
        x.addTransition(w, 3);
        x.addTransition(y, 1);

        w.addTransition(u, 5);
        w.addTransition(x, 3);
        w.addTransition(y, 1);
        w.addTransition(z, 5);

        z.addTransition(w, 5);
        z.addTransition(y, 2);

        N.add(u);
    }

    public static void initialization() {
        Node u = N.get(0);
        for(Node v: N) {
            if(v.isNeighbor(u)) {
                v.d(v.c(u));
            } else {
                v.d(Node.INFINITY_D);
            }
        }
    }

    public static void loop() {
        do {
            for(Node n: Node.getNodes()) {

            }
        } while(N.equals(Node.getNodes()));
    }
}

class Node {
    private static int acc = 0;
    public static final int INFINITY_D = 99;
    private int id;
    private int d;
    private String name;
    private ArrayList<Transition> relations;
    private static ArrayList<Node> nodes = new ArrayList<>();

    public Node(String name) {
        this.id = acc++;
        this.name = name;
        nodes.add(this);
    }

    public static ArrayList<Node> getNodes() {
        return nodes;
    }

    public void addTransition(Node n, int c) {
        relations.add(new Transition(n, c));
    }

    public boolean equals(Node n) {
        return n.id == this.id;
    }

    public boolean isNeighbor(Node n) {
        for(Transition t: relations) {
            if(t.getNode().equals(n)) {
                return true;
            }
        }
        return false;
    }

    public int c(Node n) {
        for(Transition t: relations) {
            if(t.getNode().equals(n)) {
                return t.getCost();
            }
        }
        return 0;
    }

    public void d(int d) {
        this.d = d;
    }

    public static Node getMin() {
        Node min = nodes.get(0);
        for(Node n: nodes) {
            if(min.d > n.d) {
                min = n;
            }
        }
        return min;
    }
}

class Transition {
    public int cost;
    public Node node;

    public Transition(Node node, int cost) {
        this.cost = cost;
        this.node = node;
    }

    public Node getNode() {
        return node;
    }

    public int getCost() {
        return cost;
    }
}
